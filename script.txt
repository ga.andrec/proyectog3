--TABLA CLIENTE--
create sequence uno
start with 1
increment by 1;

create trigger triguno
before insert on cliente
for each row
begin
select uno.nextval into :new.idcliente from dual;
end;
select * from CLIENTE;
INSERT INTO CLIENTE (cedula, nombres, direccion, email, password) VALUES
('0705608610', 'anthony landacay', 'pancho jacome', 'anthony-yangua@hotmail.com', 'nose');
INSERT INTO CLIENTE (cedula, nombres, direccion, email, password) VALUES
('1002090608', 'maria negrete', 'daule', 'maria-briones@hotmail.com', '1234');

--TABLA PRODUCTO--
create sequence produc
start with 1
increment by 1;

create trigger trigproduc
before insert on producto
for each row
begin
select produc.nextval into :new.codigo from dual;
end;

select * from producto;
INSERT INTO producto (nombres, descripcion, precio, stock) VALUES
('camiseta', 'color rojo', 25.99, 10);
INSERT INTO producto (nombres, descripcion, precio, stock) VALUES
('zapatos', 'nike for one', 108.99, 4);

--TABLA PAGO--
create sequence tpago
start with 1
increment by 1;

create trigger trigtpago
before insert on pago
for each row
begin
select tpago.nextval into :new.idpago from dual;
end;

select * from pago;
INSERT INTO pago (monto) VALUES
(55.99);
INSERT INTO pago (monto) VALUES
(20.99);

--TABLA COMPRAS--
create sequence scompra
start with 1
increment by 1;

create trigger trigscompra
before insert on compras
for each row
begin
select scompra.nextval into :new.idcompras from dual;
end;

select * from compras;
INSERT INTO compras (idcliente, idpago, fechacompra, monto, estado) VALUES
(2, 2, '20/08/2020', 80.50, 'pagada');

--TABLA DETALLECOMPRAS--
create sequence sdetallecompra
start with 1
increment by 1;

create trigger trigsdetallecompra
before insert on detallecompras
for each row
begin
select sdetallecompra.nextval into :new.iddetalle from dual;
end;

select * from detallecompras;
INSERT INTO detallecompras (idproducto, idcompras, cantidad, preciocompra) VALUES
(8, 2, 10, 250.50);