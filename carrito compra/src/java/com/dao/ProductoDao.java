package com.dao;

import com.Intefaces.ProductoCrud;
import com.config.Conexion;
import com.modelo.entidad.Producto;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

public class ProductoDao implements ProductoCrud {

    Connection con;
    Conexion cn = new Conexion();
    PreparedStatement ps;
    ResultSet rs;
    int r=0;
    public List buscar(String nombre) {
        List<Producto> list=new ArrayList();
        String sql = "select * from producto where Nombres like '%"+nombre+"%'";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto p=new Producto();
                p.setId(rs.getInt(1));
                p.setNombres(rs.getString(2));
                p.setImagen(rs.getString(3));
//                p.setFoto(rs.getBinaryStream(3));
                p.setDescripcion(rs.getString(4));
                p.setPrecio(rs.getDouble(5));
                p.setStock(rs.getInt(6)); 
               
                list.add(p);
            }
        } catch (Exception e) {
             System.err.print("ERROR:"+ e);
        }
        return list;
    }

    @Override
    public Producto listarId(int id) {
        Producto p = new Producto();
        //String sqll = "select DC.idDetalle, P.Foto, P.Nombres, DC.idCompras, DC.Cantidad, DC.PrecioCompra FROM detalle_compras DC inner join producto P on P.idProducto = DC.idProducto where idCompras=" + id;
      //  String sqllc = "select IdNombres,Foto,Descripcion,Precio,Stock,C.idCategoria FROM  "
        String sql = "select * from producto where IdProducto=" + id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                p.setId(rs.getInt(1));
                p.setNombres(rs.getString(2));
                p.setImagen(rs.getString(3));
//                p.setFoto(rs.getBinaryStream(3));
                p.setDescripcion(rs.getString(4));
                p.setPrecio(rs.getDouble(5));
                p.setStock(rs.getInt(6));  
                
            }
        } catch (Exception e) {
            System.err.print("ERROR:"+ e);
        }
        return p;
    }

    public List listar() {
        ArrayList<Producto> lista = new ArrayList();
        String sql = "select * from producto";
        try {  
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto p = new Producto();
                p.setId(rs.getInt(1));
                p.setNombres(rs.getString(2));
                p.setImagen(rs.getString(3));
//                p.setFoto(rs.getBinaryStream(3));
                p.setDescripcion(rs.getString(4));
                p.setPrecio(rs.getDouble(5));
                p.setStock(rs.getInt(6)); 
                   
                lista.add(p);
            }
        } catch (Exception e) {
             System.err.print("ERROR:"+ e);
        }
        return lista;
    }

    public void listarImg(int id, HttpServletResponse response) {
        String sql = "select * from producto where IdProducto=" + id;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        response.setContentType("image/*");
        try {
            outputStream = response.getOutputStream();
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                inputStream = rs.getBinaryStream("Foto");
            }
            bufferedInputStream = new BufferedInputStream(inputStream);
            bufferedOutputStream = new BufferedOutputStream(outputStream);
            int i = 0;
            while ((i = bufferedInputStream.read()) != -1) {
                bufferedOutputStream.write(i);
            }
        } catch (Exception e) {
             System.err.print("ERROR:"+ e);
        }
    }
    public int AgregarNuevoProducto(Producto p){
        String sql="insert into producto(Nombres,Foto,Descripcion,Precio,Stock,idCategoria)values(?,?,?,?,?,?)";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, p.getNombres());
            ps.setString(2, p.getImagen());
            ps.setString(3, p.getDescripcion());
            ps.setDouble(4, p.getPrecio());
            ps.setInt(5, p.getStock());
           
            ps.executeUpdate();
        } catch (Exception e) {
             System.err.print("ERROR:"+ e);
        }
        return r;
    }

    @Override
    public int edit(Producto p ) {
         String sql="update producto set Nombres=?,Foto=?,Descripcion=?,Precio=?,Stock=? where IdProducto=?";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, p.getNombres());
            ps.setString(2, p.getImagen());
            ps.setString(3, p.getDescripcion());
            ps.setDouble(4, p.getPrecio());
            ps.setInt(5, p.getStock());
            ps.setInt(6, p.getId());
            ps.executeUpdate();
        } catch (Exception e) {
             System.err.print("ERROR:"+ e);
        }
        return r;
    }

    @Override
    public int eliminar(int id) {
        int resultado=0;
        String sql="delete from producto where IdProducto="+id;
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            resultado= ps.executeUpdate();
    
        } catch (Exception e) {
            System.err.print("ERROR:"+ e);
        }
        return resultado;
    }
}
