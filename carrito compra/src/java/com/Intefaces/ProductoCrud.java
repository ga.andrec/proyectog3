
package com.Intefaces;

import com.modelo.entidad.Producto;
import java.util.List;
import javax.servlet.http.HttpServletResponse;


public interface ProductoCrud {
    public List buscar(String nombre);
    public Producto listarId(int id) ;
    public List listar();
    public void listarImg(int id, HttpServletResponse response) ;
    public int AgregarNuevoProducto(Producto p);
    public int edit(Producto p);
    public int eliminar(int id);
}
