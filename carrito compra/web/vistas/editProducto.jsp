
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
    
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link href="css/estilos.css" rel="stylesheet" type="text/css"/>        
        <title>Modificar datos productos </title>
    </head>
    <body>
                <div class="col-sm-5">
                    <div class="card">
                        <div class="card-header">
                            <h3>Modificar Producto</h3>
                        </div>                
                        <div class="card-body">
                            <form action="Controlador?accion=Actualizar" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Id</label>
                                    <input type="text" value="${p.getId()}" name="txtId" readonly="" class="form-control">
                                </div>  
                                <div class="form-group">
                                    <label>Nombres</label>
                                    <input type="text" value="${p.getNombres()}" name="txtNombre" class="form-control">
                                </div>                     
                                <div class="form-group">
                                    <label>Descripcion</label>
                                    <input type="text" value="${p.getDescripcion()}" name="txtDescripcion" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input type="text" value="${p.getPrecio()}" name="txtPrecio" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Stock</label>
                                    <input type="text" value="${p.getStock()}" name="txtStock" class="form-control">
                                </div>
                                <div class="form-group">                            
                                    <input type="file" value="${p.getImagen()}" name="txtFoto" placeholder="sad">
                                     
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-danger"  name="accion" value="Actualizar">Modificar</button>
                                    <a href="Controlador?accion=NuevoProducto">Regresar</a> 
                                </div>
                            </form>
                        </div>               
                    </div>
        </div>
    </body>
</html>
<%
    %>
